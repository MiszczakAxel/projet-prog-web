# imported libraries

from model import task
from model import user
import webapp2
from webapp  import utilities

# handlers for the web application

from webapp.SignUpHandler import *
from webapp.HomePageHandler import *
from webapp.NewTaskHandler import *
from webapp.AcceptTaskHandler import *
from webapp.CancelHandler import *
from webapp.AcceptHandler import *
from webapp.FinishHandler import *
from webapp.GiveUpHandler import *
from webapp.UserInfoHandler import *
from webapp.AssessmentHandler import *
from webapp.ViewResultsHandler import *
from webapp.TopTenHandler import *
from webapp.DocumentationHandler import *

# handlers for the API

from api.TaskListHandler import *
from api.TaskDetailsHandler import *
from api.SignInHandler import *
from api.PublishTaskHandler import *


class MainHandler(webapp2.RequestHandler):
	def get(self):
		user_cookie=self.request.cookies.get('user')
		if(user_cookie):
			login=utilities.checkUserCookie(user_cookie)
			if(login):
				self.redirect('/home_page')
			else:
				tasks=utilities.getTasksList()
				welcome_page=utilities.render_str("welcome_page.html", tasks=tasks)
				self.response.write(welcome_page)
		else:
			tasks=utilities.getTasksList()
			welcome_page=utilities.render_str("welcome_page.html", tasks=tasks)
			self.response.write(welcome_page)
	def post(self):
		login=self.request.get('login')
		password=self.request.get('pswd')
		if (login and password):
			u=user.findUserByLogin(login)
			if(u):
				if(u.checkPassword(password)):
					user_cookie=utilities.makeUserCookie(login)
					self.response.headers.add_header('Set-Cookie', str('user=%s' %user_cookie))
					self.redirect('/home_page')
				else:
					self.redirect('/')
			else:
				self.redirect('/')
		else:
			self.redirect('/')

class Logout(MainHandler):
	def get(self):
		self.response.headers.add_header('Set-Cookie', 'user=; Path=/')
		self.redirect('/')

app = webapp2.WSGIApplication([
    							('/', MainHandler),
                               	("/sign_up",SignUpHandler),
                               	('/logout',Logout),
                               	('/home_page',HomePageHandler),
                               	('/new_task', NewTaskHandler),
                               	("/accept_task", AcceptTaskHandler),
                               	('/cancel',CancelHandler),
                               	("/accept", AcceptHandler),
                               	("/finish", FinishHandler),
                               	("/give_up", GiveUpHandler),
                               	('/user_info', UserInfoHandler),
                               	('/assess', AssessmentHandler),
                               	('/top_ten', TopTenHandler),
                               	("/view_results", ViewResultsHandler),
                               ('/documentation', DocumentationHandler),
								('/api/task_list', TaskListHandler),
                               	("/api/task_details", TaskDetailsHandler),
                               	("/api/sign_in", SignInHandler),
                               	("/api/publish_task", PublishTaskHandler)], debug=True)