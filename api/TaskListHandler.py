# api handler for requesting the list of tasks from the database

import webapp2
import json
from model import task

class TaskListHandler(webapp2.RequestHandler):
	def get(self):
		tasks=task.findAllTasks()
		titles=[]
		for t in tasks:
			titles.append(t.title)
		titles=json.dumps(titles)
		self.response.write(titles)