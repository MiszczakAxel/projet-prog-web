# handler for requesting an task by a given title

import webapp2
from model import task
from webapp import utilities
import json

class TaskDetailsHandler(webapp2.RequestHandler):
	def get(self):
		title=self.request.get('title')
		dic={}
		if(title):
			title=utilities.url2title(title)
			t=task.findTaskByTitle(title)
			if(t):
				dic['title']=t.title
				dic['description']=t.description
				dic['publisher']=t.publisher
				dic['status']=t.getStatus()
		dic=json.dumps(dic)
		self.response.write(dic)