# this module provides the features
# that allows an user to log himself on the API

from model import user
import webapp2
from webapp import utilities
import json

class SignInHandler(webapp2.RequestHandler):
	def post(self):
		dic={}
		dic["status"]="error"
		login=self.request.get('login')
		password=self.request.get('pswd')
		if (login and password):
			u=user.findUserByLogin(login)
			if(u):
				if(u.checkPassword(password)):
					user_cookie=utilities.makeUserCookie(login)
					self.response.headers.add_header('Set-Cookie', str('user=%s' %user_cookie))
					dic["status"]="ok"
		dic=json.dumps(dic)
		self.response.write(dic)