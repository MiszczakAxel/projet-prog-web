# handler for publishing a new task
# by the API

from model import task
from model import user
from webapp import utilities
import webapp2
import json

class PublishTaskHandler(webapp2.RequestHandler):
	def post(self):
		dic={}
		dic["status"]="error"
		user_cookie=self.request.cookies.get('user')
		if(user_cookie):
			login=utilities.checkUserCookie(user_cookie)
			if(login):
				title=self.request.get('title')
				description=self.request.get('description')
				if (title and description):
					task.insertTask(title,description,login)
					utilities.getTasksList(True)
					dic["status"]="ok"
		dic=json.dumps(dic)
		self.response.write(dic)