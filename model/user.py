import hmac
import random
import string
from google.appengine.ext import db

# define the class User
# which describes a User in the web application
# This module also provide functions to manipulate Users

class User(db.Model):
	login=db.StringProperty(required=True)
	salt=db.StringProperty(required=True)
	password=db.StringProperty(required=True)
	score=db.IntegerProperty(required=True)
	email=db.EmailProperty(required=False)
	phone_number=db.PhoneNumberProperty(required=False)
	cell_phone_number=db.PhoneNumberProperty(required=False)
	def checkPassword(self,psw):
		hash_psw=hmac.new(str(self.salt),psw).hexdigest()
		return str(self.password)==hash_psw
 
def insertUser(login,password):
# hash and salt the password
	salt=''.join(random.choice(string.letters) for x in xrange(5))
	password=hmac.new(salt,password).hexdigest()
# create and insert the user
	u=User(login=login,salt=salt,password=password, score=0)
	u.put()

# find a busier by his login

def findUserByLogin(login):
	u=db.GqlQuery("select * from User where login='%s'" %login)
	u=list(u)
	if(len(u)>0):
		return u[0]
	else:
		return None

# get the list of the ten best workers 

def getTopTenList():
	top_ten=db.GqlQuery('select * from User order by score desc limit 10')
	top_ten=list(top_ten)
	return top_ten