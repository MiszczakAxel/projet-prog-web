from google.appengine.ext import db

# module task
# define the class Task
# which describes a task in the web application
# This module also provide functions to manipulate tasks

class Task(db.Model):
	title=db.StringProperty(required=True)
	description=db.TextProperty(required=True)
	status=db.StringProperty(required=True)
	post_date=db.DateTimeProperty(auto_now_add = True)
	publisher=db.StringProperty(required=True)
	executer=db.StringProperty(required=False)
	quality=db.StringProperty(required=False)
	assessment=db.TextProperty(required=False)
	def title2url(self):
		title=str(self.title)
		return title.replace(' ','+')
	def getStatus(self):
		if(self.status=='N'):
			return 'not accepted yet'
		elif(self.status=='A'):
			return 'accepted'
		elif(self.status=='F'):
			return 'finished'
	def setExecuter(self,executer):
		self.executer=executer
		self.status='A'
	def removeExecuter(self):
		self.executer=None
		self.status='N'

# get all tasks from the database

def findAllTasks():
	tasks=db.GqlQuery("SELECT * FROM Task")
	tasks=list(tasks)
	return tasks

# find tasks that have not been accepted

def findTasksWithoutExecuter():
	tasks=db.GqlQuery("select * from Task where status='N'")
	tasks=list(tasks)
	return tasks
# get the tasks published by a given user

def findTasksByPublisher(publisher):
	tasks=db.GqlQuery("select * from Task where publisher='%s'" %publisher)
	tasks=list(tasks)
	return tasks

# get the tasks that a given user has accepted to execute

def findTasksByExecuter(executer):
	tasks=db.GqlQuery("select * from Task where executer='%s' and status='A'" %executer)
	tasks=list(tasks)
	return tasks


# get the tasks that the user has finished

def findFinishedTasksByExecuter(executer):
	tasks=db.GqlQuery("select * from Task where executer='%s' and status='F'" %executer)
	tasks=list(tasks)
	return tasks


# find a task from a given title

def findTaskByTitle(title):
	t=db.GqlQuery("select * from Task where title='%s'"%title)
	t=list(t)
	if(len(t)>0):
		return t[0]
	else:
		return None

# insert a new task in the database

def insertTask(title,description,publisher):
	t=Task(title=title,description=description,publisher=publisher,status='N')
	t.put()