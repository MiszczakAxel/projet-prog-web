# class handling request
# for updating the contact info of an user

from model import user
import utilities
import webapp2
import hmac

class UserInfoHandler (webapp2.RequestHandler):
	def get(self):
		user_cookie=self.request.cookies.get('user')
		if(user_cookie):
			login=utilities.checkUserCookie(user_cookie)
			if(login):
				u=user.findUserByLogin(login)
				user_info_page=utilities.render_str('user_info_page.html', u=u)
				self.response.write(user_info_page)
			else:
				self.redirect('/')
		else:
			self.redirect('/')
	def post(self):
		user_cookie=self.request.cookies.get('user')
		if(user_cookie):
			login=utilities.checkUserCookie(user_cookie)
			if(login):
				u=user.findUserByLogin(login)
				password=self.request.get('password')
				if(password):
					u.password=hmac.new(str(u.salt),password).hexdigest()
				else:
					self.redirect('/user_info')
				if(self.request.get('email')):
					u.email=self.request.get('email')
				if(self.request.get('phone_number')):
					u.phone_number=self.request.get('phone_number')
				if(self.request.get('cell_phone_number')):
					u.cell_phone_number=self.request.get('cell_phone_number')
				u.put()
				self.redirect('/home_page')
			else:
				self.redirect('/')
		else:
			self.redirect('/')