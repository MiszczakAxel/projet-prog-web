import webapp2
from model import user
import utilities

class SignUpHandler(webapp2.RequestHandler):
	def get(self):
		sign_up_page=utilities.render_str('sign_up_page.html')
		self.response.write(sign_up_page)
	def post(self):
		login=self.request.get("username")
		pswd1=self.request.get("pswd1")
		pswd2=self.request.get("pswd2")
		if(login and pswd1 and pswd2):
			if(pswd1==pswd2):
				user.insertUser(login,pswd1)
				cookie=utilities.makeUserCookie(login)
				self.response.headers.add_header('Set-Cookie', str('user=%s' %cookie))
				self.redirect("/home_page")
			else:
				self.redirect("/sign_up")
		else:
			self.redirect("/sign_up")
