# handler for the top ten's list of best workers
# define only the get method for consulting this list

from model import user
import utilities
import webapp2

class TopTenHandler(webapp2.RequestHandler):
	def get(self):
		top_ten=user.getTopTenList()
		top_ten_page=utilities.render_str('top_ten_page.html', top_ten=top_ten)
		self.response.write(top_ten_page)