# handler for publishing a new task

from model import task
from model import user
import utilities
import webapp2

class NewTaskHandler(webapp2.RequestHandler):
	def get(self):
		user_cookie=self.request.cookies.get('user')
		if(user_cookie):
			login=utilities.checkUserCookie(user_cookie)
			if(login):
				new_task_page=utilities.render_str('new_task_page.html')
				self.response.write(new_task_page)
			else:
				self.redirect('/')
		else:
			self.redirect('/')
	def post(self):
		user_cookie=self.request.cookies.get('user')
		if(user_cookie):
			login=utilities.checkUserCookie(user_cookie)
			if(login):
				title=self.request.get('title')
				description=self.request.get('description')
				if (title and description):
					task.insertTask(title,description,login)
					utilities.getTasksList(True)
					self.redirect('/home_page')
				else:
					self.redirect('/new_task')
			else:
				self.redirect('/')
		else:
			self.redirect('/')