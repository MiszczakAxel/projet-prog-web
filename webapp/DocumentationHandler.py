# handler for the documentation of tha api

import webapp2
import utilities

class DocumentationHandler(webapp2.RequestHandler):
	def get(self):
		documentation_page=utilities.render_str('documentation_page.html')
		self.response.write(documentation_page)
