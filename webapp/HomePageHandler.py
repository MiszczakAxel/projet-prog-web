import webapp2
from model import task
from model import user
import utilities

class HomePageHandler(webapp2.RequestHandler):
	def get(self):
		user_cookie=self.request.cookies.get('user')
		if(user_cookie):
			login=utilities.checkUserCookie(user_cookie)
			if(login):
				u=user.findUserByLogin(login)
				user_tasks=task.findTasksByPublisher(login)
				accepted_tasks=task.findTasksByExecuter(login)
				home_page=utilities.render_str('home_page.html', user_tasks=user_tasks, accepted_tasks=accepted_tasks, u=u)
				self.response.write(home_page)
			else:
				self.redirect('/')
		else:
			self.redirect('/')