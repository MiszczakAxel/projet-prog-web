# handler for giving an accepte task up
# verify that the user is well logged

from model import task
import utilities
import webapp2

class GiveUpHandler(webapp2.RequestHandler):
	def post(self):
		user_cookie=self.request.cookies.get('user')
		if(user_cookie):
			login=utilities.checkUserCookie(user_cookie)
			if(login):
				title=self.request.get('title')
				title=utilities.url2title(title)
				t=task.findTaskByTitle(title)
				t.removeExecuter()
				t.put()
				utilities.getTasksList(True)
				self.redirect('/home_page')
			else:
				self.redirect('/')
		else:
			self.redirect('/')