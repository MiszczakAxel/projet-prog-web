# handler for requesting the list of tasks that can be accepted

from model import task
import utilities
import webapp2

class AcceptTaskHandler (webapp2.RequestHandler):
	def get(self):
		user_cookie=self.request.cookies.get('user')
		if(user_cookie):
			login=utilities.checkUserCookie(user_cookie)
			if(login):
				tasks=task.findTasksWithoutExecuter()
				accept_task_page=utilities.render_str('accept_task_page.html', tasks=tasks)
				self.response.write(accept_task_page)
			else:
				self.redirect('/')
		else:
			self.redirect('/')