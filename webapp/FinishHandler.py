# handler for finishing an accepted task
# verify that the user is well logged
# and that he's the publisher of the task

from model import task
import utilities
import time
import webapp2

class FinishHandler(webapp2.RequestHandler):
	def post(self):
		user_cookie=self.request.cookies.get('user')
		if(user_cookie):
			login=utilities.checkUserCookie(user_cookie)
			if(login):
				title=self.request.get('title')
				title=utilities.url2title(title)
				t=task.findTaskByTitle(title)
				t.status='F'
				t.put()
				time.sleep(1)
				self.redirect('/home_page')
			else:
				self.redirect('/')
		else:
			self.redirect('/')