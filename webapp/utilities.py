# this module provides some utilities
# that are usefull in several features of the web application

from model import user
from model import task
import os
import time
import hashlib
import jinja2

from google.appengine.api import memcache


template_dir = os.path.join(os.path.dirname(__file__), 'templates')
jinja_env = jinja2.Environment(loader = jinja2.FileSystemLoader(template_dir), autoescape=True)

# make the cookie for a givven user's login

def makeUserCookie(login):
	hash_login=hashlib.sha256(login).hexdigest()
	return '%s|%s'%(hash_login,login)

# check that an user cookie is correct

def checkUserCookie(user_cookie):
	tab=user_cookie.split('|')
	hash_login=hashlib.sha256(tab[1]).hexdigest()
	if(tab[0]==hash_login):
		return tab[1]
	else:
		return None

# render a given template

def render_str(template, **params):
    t = jinja_env.get_template(template)
    return t.render(params)


# translate a task's title from url to plein text

def url2title(title):
	return title.replace('+', ' ')

# get the list of all tasks from the cache
# and update it if necessary

def getTasksList(update=False):
	key="tasklist"
	tasks=memcache.get(key)
	if(update or tasks is None):
		time.sleep(1)
		tasks=task.findTasksWithoutExecuter()
		memcache.set(key, tasks)
	return tasks
