# handler for assessment request
# the get method returns the text html to assess a task
# and the post method update the assessment for the task

from model import user
from model import task
import utilities
import webapp2

class AssessmentHandler(webapp2.RequestHandler):
	def get(self):
		user_cookie=self.request.cookies.get('user')
		if(user_cookie):
			login=utilities.checkUserCookie(user_cookie)
			if(login):
				assessment_page=utilities.render_str('assessment_page.html', title=self.request.get('title'))
				self.response.write(assessment_page)
			else:
				self.redirect('/')
		else:
			self.redirect('/')
	def post(self):
		user_cookie=self.request.cookies.get('user')
		if(user_cookie):
			login=utilities.checkUserCookie(user_cookie)
			if(login):
				quality=self.request.get('quality')
				assessment=self.request.get('assessment')
				if(quality and assessment):

# update the information about the task

					title=self.request.get('title')
					title=utilities.url2title(title)
					t=task.findTaskByTitle(title)
					t.assessment=assessment
					t.quality=quality
					t.put()

# update the score of the executer

					executer=str(t.executer)
					u=user.findUserByLogin(executer)

					if(executer!=login and quality=='good'):
						u.score+=10
					elif(executer!=login and quality=='bad'):
						u.score-=5

					u.put()
					self.redirect('/home_page')
				else:
					self.redirect('/assess?title='+self.request.get('title'))
			else:
				self.redirect('/')
		else:
			self.redirect('/')