# handler for viewing user's results
# this handler only defines the get method
# to display the list of tasks executed by the user with their assessment

from model import task
from model  import user
import utilities
import webapp2

class ViewResultsHandler(webapp2.RequestHandler):
	def get(self):
		user_cookie=self.request.cookies.get('user')
		if(user_cookie):
			login=utilities.checkUserCookie(user_cookie)
			user_login=self.request.get('user')
			if(user):
				finished_tasks=task.findFinishedTasksByExecuter(user_login)
				u=user.findUserByLogin(user_login)
				if(login):
					results_page=utilities.render_str('results_page.html', finished_tasks=finished_tasks, u=u, login=True, tasks_number=len(finished_tasks))
				else:
					results_page=utilities.render_str('results_page.html', finished_tasks=finished_tasks, u=u, login=False, tasks_number=len(finished_tasks))
				self.response.write(results_page)
			else:
				self.redirect('/')