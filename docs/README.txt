﻿Le site réalisé dans le cadre du projet de développement web a pour titre Web Task. Conformément au cahier des charges, il s’agit d’un site de publication de tâches réalisé en python et déployé sur le Google App Engine. Ce rapport présente les grandes lignes du code et des fonctionnalités mises en œuvre dans le cadre du projet. Précisons également que l’application est disponible sur Internet à l’adresse suivante :

http://web-tasks-tncy.appspot.com/

Ce rapport est organisé en trois parties distinctes : premièrement, un descriptif des différentes fonctionnalités offertes aux utilisateurs par Web Task ; en seconde partie, nous aborderons des questions liées à l’organisation des sources et aux choix techniques qui ont été faits lors du développement ; la troisième partie fera état de l’apport de chacun au cours du projet et des difficultés qui ont été rencontrées ; finalement, la conclusion fera le point sur le projet, ses enjeux et d’éventuels développements ultérieurs.

1. Utilisation de Web Task

Cette section présente la liste exhaustive des fonctionnalités que Web Task apporte aux utilisateurs. Chaque fonctionnalité sera présentée sous la forme d’une histoire retraçant le cheminement de l’utilisateur. Le plan adopté dans cette section reprend la structuration des fonctionnalités en deux blocs : premièrement, nous aborderons les services proposés par l’application web ; après cela, nous présenterons les possibilités d’accès à Web Task au travers de l’API.

1.1. L’application web

Cette sous-section présente les fonctionnalités disponibles aux utilisateurs via leur navigateur Internet. Chaque fonctionnalité est présentée sous la forme d’une histoire utilisateur ; si une ou plusieurs histoires doivent être réalisées préalablement à une autre, celles-ci sont indiquées avant d’aborder le scénario.

1.1.1. Accès à la page d’accueil

Depuis son navigateur web, un utilisateur saisit l’URL correspondant à Web Task. Le site charge alors la liste des tâches publiées mais non acceptées, génère la page d’accueil et la transmet au client web de l’utilisateur.

1.1.2. Inscription d’un utilisateur

Depuis la page d’accueil, un utilisateur clique sur le lien Sign up pour créer un compte auprès de Web Task. Le site charge alors le formulaire de création de compte et le transmet au client de l’utilisateur. Ce dernier saisit alors les informations demandées et clique sur le bouton de création de compte. Plusieurs cas sont alors possibles :

- si l’utilisateur n’a pas rempli tous les champs, alors le site lui redemande la saisie ;

- s’il n’a pas écrit la même chose dans les champs Password et Password verification, alors le site lui redemande la saisie ;

- s’il a correctement rempli le formulaire, alors le site enregistre son compte dans la base de données, crée et envoie un cookie authentifiant l’utilisateur et le redirige sur sa page personnelle.

1.1.3. Authentification d’un utilisateur

Avant cette histoire, l’utilisateur doit avoir créé un compte auprès de Web Task.

Depuis la page d’accueil du site, un utilisateur saisit les informations permettant son authentification dans le champ prévu à cet effet. Plusieurs cas sont alors possibles :

- si l’utilisateur n’a pas rempli tous les champs, alors le site affiche la page d’accueil du site ;

- si l’utilisateur a saisit un login incorrect, alors le site affiche la page d’accueil ;

- si l’utilisateur a correctement saisi son login mais a fait une erreur dans son mot de passe, alors le site affiche la page d’accueil ;

- enfin, si l’utilisateur a correctement saisi son login et son mot de passe, alors le site crée un cookie identifiant le client de l’utilisateur et redirige ce dernier sur sa page personnelle.

1.1.4. Accès à la page personnelle

Avant de pouvoir accéder à cette fonctionnalité, l’utilisateur doit s’être authentifié, soit en créant un compte, soit en remplissant le formulaire d’authentification.

Lorsqu’un utilisateur tente d’accéder à sa page personnelle, plusieurs scénarios sont possibles :

- si son navigateur web dispose du cookie d’authentification, alors le site récupère la liste des tâches publiées par l’utilisateur, la liste des tâches qu’il a acceptées et génère la page personnelle de l’utilisateur ;

- sinon, l’utilisateur est redirigé sur la page d’accueil pour s’authentifier.

1.1.5. Publication d’une nouvelle tâche

Pour disposer de cette fonctionnalité, un utilisateur doit s’être préalablement authentifié.

Depuis sa page personnelle, un utilisateur clique sur le lien Publish a new task. Le site charge alors le formulaire de saisie de tâche qu’il transmet au client de l’utilisateur. Ce dernier saisit les informations demandées et clique sur le bouton Publish. Plusieurs scénarios sont alors possibles :

- si l’utilisateur n’a pas saisi tous les champs, alors le site lui redemande la saisie ;
- si l’utilisateur a correctement saisi tous les champs, alors le site insère la nouvelle tâche dans la base de données et redirige l’utilisateur sur sa page personnelle où la nouvelle tâche apparaît.

1.1.6. Suppression d’une tâche

Pour disposer de cette fonctionnalité, un utilisateur doit s’être préalablement authentifié et doit avoir publié au moins une tâche.

Depuis sa page personnelle, un utilisateur clique sur le bouton cancel qui se trouve sous le titre d’une tâche. Le site supprime alors la tâche de la base de données et redirige l’utilisateur vers sa page personnelle où la tâche ne figure plus.

1.1.7. Acceptation d’une nouvelle tâche

Pour disposer de cette fonctionnalité, un utilisateur doit s’être préalablement authentifié auprès du service.

Depuis sa page personnelle, un utilisateur clique sur le lien lui proposant d’accepter une nouvelle tâche. Le site charge alors la liste des tâches qui n’ont pas encore été acceptées et génère la page d’acceptation qu’il transmet au navigateur du client. Plusieurs scénarios sont alors possibles :

- l’utilisateur clique sur l’un des liens de la section navigation et quitte la page d’acceptation de tâches ;

- l’utilisateur clique sur le bouton lui permettant d’accepter une tâche. Le site retire alors cette tâche de la liste des tâches disponibles, l’ajoute à celles déjà acceptées par l’utilisateur et le réoriente sur la page d’acceptation de tâches.

1.1.8. Abandon d’une tâche

Pour accéder à cette fonctionnalité, un utilisateur doit s’être préalablement authentifié auprès de Web Task et doit avoir accepter au moins une tâche.

Depuis sa page personnelle, un utilisateur clique sur le bouton lui permettant d’abandonner une tâche. Celle-ci est alors retirée des tâches acceptées par l’utilisateur et réintroduite dans la liste des tâches libres. L’utilisateur perd alors 5 points pour s’être désisté d’une tâche et il est réorienté sur sa page personnelle où la tâche a disparue.

1.1.9. Validation d’une tâche

Pour accéder à cette fonctionnalité, un utilisateur doit s’être authentifié auprès du service est avoir accepté au moins une tâche.

Depuis sa page personnelle, un utilisateur clique sur le bouton lui permettant d’indiquer qu’il a fini le travail qu’il avait accepté. Le site met alors à jour les informations en base de données et réoriente l’utilisateur sur sa page personnelle où la tâche ne figure plus.

1.1.10. Évaluation d’une tâche

Pour accéder à cette fonctionnalité, un utilisateur doit s’être authentifié, avoir publié au moins une tâche qui a été validée par un autre utilisateur.

Depuis sa page personnelle, un utilisateur clique sur le lien lui permettant d’évaluer une tâche. Le site lui transmet alors le formulaire d’évaluation de tâche. Après avoir saisi les différentes informations qui lui sont demandées, l’utilisateur clique sur le bouton lui permettant de valider son évaluation. Plusieurs scénarios sont alors possibles :

- si l’utilisateur n’a pas correctement rempli tous les champs, alors le site lui redemande la saisie ;

- s’il a bien saisi toutes les informations relatives à l’évaluation, alors ces informations sont ajoutées à la tâche. Le capital point de l’utilisateur ayant réalisé la tâche est augmenté ou diminué en fonction de l’évaluation et l’utilisateur est redirigé sur sa page personnelle où la tâche a définitivement disparu.

1.1.11. Consultation des résultats

Pour accéder à cette fonctionnalité, un utilisateur doit s’être authentifié auprès de Web Task.

Depuis sa page personnelle, un utilisateur clique sur le lien lui permettant de consulter ses résultats. Le site génère alors la page contenant le score de l’utilisateur ainsi que la liste des tâches qu’il a acceptées et qui ont été évaluées et la transmet au navigateur de l’utilisateur.

1.1.12. Consultation du top 10 des meilleurs travailleurs

Depuis la page d’accueil du site, un utilisateur clique sur le lien lui offrant la possibilité de voir les 10 meilleurs travailleurs. Le site génère alors la page demandée par l’utilisateur et la transmet à son navigateur. L’utilisateur peut voir davantage de détails sur le travail de chacun des 10 personnes indiquées en cliquant sur leur login.

1.1.13. Consultation de la documentation de l’API

Depuis la page d’accueil du site, un utilisateur clique sur le lien lui permettant de visualiser la documentation de l’API. La page est alors chargée avant d’être transmise au navigateur de l’utilisateur.

1.2. Fonctionnalités de l’API

Cette sous-section présente les possibilités d’accès à Web Task par l’intermédiaire de l’API. Chaque fonctionnalité est présentée suivant le même formalisme que dans la section précédente.

1.2.1. Consultation de la liste des tâches

Un service transmet à Web Task la requête HTTP demandant la liste des tâches publiées sur le site. Web Task charge alors ladite liste et génère une chaîne JSON contenant les titres de toutes les tâches publiées mais non encore acceptées ; il transmet ensuite cette chaîne au service.

1.2.2. Consultation des informations d’une tâche donnée

Un service adresse la requête concernant une tâche donnée. Web Task récupère alors la tâche en base de données et génère une chaîne JSON contenant les détails à propos de celle-ci avant de transmettre la réponse au service.

1.2.3. Authentification par l’API

Pour accéder à cette fonctionnalité, un utilisateur doit avoir préalablement créé un compte.

Un service transmet une requête d’authentification à Web Task. Plusieurs cas sont alors possibles :

- si le service n’a pas transmis toute l’information requise, il reçoit le statut d’erreur ;

- s’il a transmis de l’information erronée, il reçoit le statut d’erreur ;

- s’il a bien transmis son login et son Password, alors il reçoit le statut de validation accompagné d’un cookie lui permettant de s’authentifier.

1.2.4. Publication d’une tâche

Pour accéder à cette fonctionnalité, le service doit s’être authentifié auprès de Web Task.

Un service transmet la requête de publication de tâche à Web Task. Plusieurs cas sont alors possibles :

- si le service n’a pas ajouté le cookie d’authentification à sa requête, il reçoit un statut d’erreur ;

- si le cookie transmis par le service est erroné, il reçoit un statut d’erreur ;

- si le service n’a pas transmis toute l’information nécessaire, il reçoit un statut d’erreur ;

- si le service transmet toute l’information nécessaire, il reçoit le statut de validation.

2. Aspects techniques

Cette section traite des aspects techniques abordés au cours du projet. Dans un premier temps, il est fait état du découpage des différentes classes du projet ; ensuite, nous aborderons les questions relatives à la sécurité, au cache et à la base de données.

2.1. Structuration des sources

Comme indiqué en introduction, ce projet a été réalisé en python et déployé avec le Google App Engine. Après plusieurs tâtonnements, nous avons finalement opté pour une structuration MVC des sources du projet. Ce choix nous a permis de clairement séparer les aspects liés au modèle de notre application de ceux liés au protocole HTTP. Les paragraphes de cette section aborderont dans l’ordre le modèle de notre application, ses vues et enfin ses contrôleurs.

Le modèle de notre application est constitué des classes user et task qui décrivent respectivement des utilisateurs et des tâches. Ces classes font partie du package Model présent dans le répertoire source du projet. Elles fournissent différentes fonctions et méthodes permettant la manipulation et la consultation des données ainsi que leur enregistrement en base de données.

Du fait de la nature orientée web de notre application, les vues sont implantées sous la forme de pages HTML. Ces pages sont présentes dans le répertoire /webapp/templates. Certaines d’entre elles contiennent des balises Jinja2, le langage de rendering fourni par Python. L’utilisation de Jinja2 nous a permis de générer facilement des pages dynamiques sans avoir à intégrer des aspects liés à la vue dans les contrôleurs de notre application.

Enfin, les contrôleurs de notre application sont représentés par deux packages : webapp et api. Ces deux répertoires sont présents à la racine du projet et représentent respectivement les contrôleurs liés à l’application web et à l’API de Web Task. Ces deux packages contiennent toute une série de classes de contrôleur répondant chacun à l’une des fonctionnalités listées dans la section 1.

2.2. Autres aspects techniques

Cette section aborde les autres points techniques qui ont influencé notre développement. Chacun de ces points fait l’objet d’un paragraphe : dans un premier temps, nous ferons état des choix concernant la base de données ; après cela, nous aborderons les choix de gestion du cache ; enfin, nous présenterons les choix faits en faveur de la sécurité de notre site.

La base de données utilisée au sein de Web Task est la base de données nativement fournie par le Google App Engine. La raison de ce choix est la simplicité d’utilisation de l’outil : étant nativement inséré, il n’est pas besoin d’ajouter de pilotes supplémentaires pour y avoir accès. D’autre part, le Google App Engine fournit une série de classes et de fonctionnalités qui évitent d’avoir à rédiger de nombreuses requêtes d’accès à la base de données. Enfin, le langage GQL utilisé par cet outil simplifie grandement la rédaction de requêtes par rapport au traditionnel SQL.

Pour accélérer la gestion de certaines requêtes, nous avons choisi d’avoir recours à la mise en cache de certains résultats. L’outil utilisé pour ceci est memcache pour lequel il existe une abondante documentation sur Internet. Nous avons choisi de mettre en cache la liste des tâches publiées mais non encore acceptées : la raison en est que cette liste est très souvent demandée puisqu’elle apparaît dans plusieurs pages du site dont la page d’accueil. Cette abondance de requêtes risque de ralentir l’utilisation de notre service s’il est nécessaire d’accéder à la base de données pour répondre à chacune d’entre elles, d’où la décision de mettre la liste en cache.

Enfin, le dernier point technique abordé dans cette section est celui de la sécurité. Pour permettre d’assurer l’authentification des utilisateurs avant l’accès à de nombreuses fonctionnalités, nous avons eu recours à l’utilisation d’un cookie. Ce dernier est encodé par des algorithmes de cryptage, ce qui permet de s’assurer que seuls des utilisateurs s’étant bien authentifiés auprès de notre service accèdent au site. Pour protéger les mots de passe au sein de la base de données, nous avons eu recours à la mise en place d’algorithmes de hashage et à la génération aléatoire de salts. Ce choix technique nous permet d’assurer une meilleur protection aux informations publiées par nos utilisateurs.

3. Apports des différents participants :

Cette section aborde les apports réalisés par chaque membre de l’équipe de travail ainsi que les difficultés qu’ils ont rencontrées. Chacune des section de cette partie présentera les apports réalisés par l’un des membres du projet.

3.1. Responsable du projet

Le responsable du projet était en charge de la rédaction des User stories ainsi que de la validation du planning. Pour répondre à cette mission, il a rédigé une liste de User stories qu’il a soumis à la validation de ses collègues et à celle de l’enseignant encadrant le projet. Sa position de chef de projet lui a permis d’encadrer le travail de l’équipe. Cette dernière expérience aura été particulièrement profitable puisqu’elle aura permis au chef de projet de mieux appréhender les relations au sein d’une équipe de travail.

Les difficultés rencontrées par le chef de projet furent principalement d’ordre technique. Étant débutant en matière de programmation web en python, il lui a fallu découvrir de nouveaux aspects de programmation et les mettre en œuvre au sein de ce projet. Cette expérience aura été particulièrement profitable et a déjà pu être réinvestie dans un autre projet réalisé en Java.

3.2. Responsable du dépôt et du site

J'étais en charge de la surveillance des commits de la part des participants, ainsi que de la gestion du site via Google App Engine. J'ai donc surveillé les commits des participants, et ce via les mails que je recevais à chaque fois qu'une personne faisait un commit sur le projet et via BitBucket. Dès que le projet a été reconnu comme fonctionnel, je l'ai uploadé via Google App Engine sur le lien fourni plus tôt (qui est : http://web-tasks-tncy.appspot.com/).

La plus grosse difficulté rencontrée a été l'absence quasi totale sur le projet d'un de nos camarades qui a effectué 1 seul commit et qui n'a pas communiqué par la suite. L'autre problème, plus personnel et qui n'en est pas un pour tous, a été la fréquence élevée de commit de la part d'un autre membre. Il a réalisé presque tout le code source à lui seul, et ce code était parfaitement fonctionnel. Une autre difficulté a été de reprendre tout l'affichage du site. En effet, le créateur de ces documents de style étant déficient visuel, il lui était impossible de vérifier l'affichage du site. Il m'a donc fallu reprendre tous les fichiers afin de tout réorganiser, et aussi approfondir mes connaissances sur le CSS afin de rendre le tout plus agréable à l'oeil. Mais tout cela m'a été profitable et me sera profitable par la suite, le CSS étant un langage très utilisé dans la programmation web.

Conclusion :

Pour conclure ce rapport, rappelons que Web Task est un site web permettant à des utilisateurs de publier en ligne des travaux que d’autres utilisateurs pourront effectuer. Ce projet a été développé en python avec l’appui du Google App Engine, l’utilitaire de développement web mis à disposition par Google. Ce projet a permis une mise en œuvre de l’architecture MVC ainsi que de différentes technologies et techniques liées au développement de sites web.

Web Task pourra encore faire l’objet de développements ultérieurs. Il pourrait notamment être intéressant de remplacer l’utilisation des cookies réglementée par la législation française par celle de sessions pour lesquelles il existe un cadre légal beaucoup plus large. Il pourrait également être intéressant d’ajouter des traitements par JavaScript pour effectuer par exemple le cryptage des mots de passe sur le navigateur de l’utilisateur avant retransmission à notre serveur.