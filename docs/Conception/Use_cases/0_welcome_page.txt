When an user arrives on the web site :

- the application get the list of all tasks stored in the database ;
- the main renders the welcome page and display the list of all tasks ;
- the main send the welcome page to the client.