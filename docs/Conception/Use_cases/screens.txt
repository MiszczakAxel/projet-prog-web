welcome page :

at the top, a bar with the navigation buttons :

- sign up ;
- sign in ;
- top ten.

in the middle, we see the list of the published tasks
with their title, a short description of the at tented work
and eventually an evaluation comment

user sign up :

at the top, the navigation bar with the following buttons :

- welcome page ;
- top ten

in the middle, two text fields
for the login and the password
the field for the password is in password mode
a button OK to validate the sign up

user home page :

at the top :

- welcome page ;
- top ten
- post a new task.
- accept a new task
display settings

in the middle a first header Published tasks
with a list of the tasks that the user published
for each task, we display the title, the dead line (if available), the state (posted, accepted, finished)
the name of the user(s) who accepted the task
and a link result evaluation if the task is finished

Under this section, a second section called Accepted tasks
with the list of the tasks that the user has accepted
a button finish for each of these tasks
and a button give up to give up the task

in the up right corner is the user’s picture
and the page is rendered with the display user’s settings

new task screen :

- at the top :

- welcome page ;
-top ten
- home page ;

in the middle, a field for the title of the task
a text area for the task description.
a text field for the dead line
a field to type how many times the task can be accepted
and a button finish to submit the task

evaluation screen :

at the top are the links :

welcome page
- top ten
home page.

in the middle is a place to type the note
the bonus/malus for the user who finished the task
a text field to add a comment
and a button Ok to submit the evaluation

Top ten screen :

at the top :

- welcome page ;
- home page (if logged)

In the middle, a list of the ten best users
with a label for the best task publisher
and for the best worker

Display settings :

at the top of the page :

welcome page
top ten
home page ;

at the middle are the display settings :

- font family ;
- font size ;
- font color ;
- background color ;

hinder those settings is a button to choose a picture for the user’s home page
finally, there is an OK button